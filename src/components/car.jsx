import React from 'react';

function Car(props) {
  return (
    <div className="car">
      <h3>{props.name}</h3>
      <p>
        Year: <strong>{props.year}</strong>
      </p>
    </div>
  );
}

export default Car;