import React from 'react';

export function NoteBook(props) {
  return (
    <div className="notebook">
      <h3>Модель {props.name}</h3>
      <p>
        Цена: <strong>{props.price}</strong>
      </p>
      <p>{Math.random()}</p>
    </div>
  );
}
 