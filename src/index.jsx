import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import { Car } from './components/App';
import { NoteBook } from './components/App';

const App = (
  <>
    <div className="MyApp">
      <div>
        <Car name="Ford Focus" year="2017" />
        <Car name="Audi A8" year="2015" />
        <Car name="Mazda 3" year="2010" />
      </div>
      <div>
        <NoteBook name="asdasd" price="2012312317" />
        <NoteBook name="asdasd" price="201231215" />
        <NoteBook name="asdasd" price="2010123" />
      </div>
    </div>
  </>
);

ReactDOM.render(App, document.querySelector('#root'));
